package com.example.pdf.util;

import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.encryption.InvalidPasswordException;
import org.apache.pdfbox.text.PDFTextStripper;
import org.apache.pdfbox.text.PDFTextStripperByArea;

import java.io.File;
import java.io.IOException;

public class PDFUtils {


    public static void main(String[] args) {

        File file=new File("D:\\test.pdf");

        try (PDDocument document = PDDocument.load(file)) {
            document.getClass();
            if(!document.isEncrypted()) {
                //读取pfd文件中的文字
                PDFTextStripperByArea stripper = new PDFTextStripperByArea();
                stripper.setSortByPosition(true);
                PDFTextStripper tStripper = new PDFTextStripper();
                String pdfFileInText = tStripper.getText(document);
                String[] lines = pdfFileInText.split("\\r?\\n");
                //输出到word
                TransformationWord transformationWord=new TransformationWord();
                transformationWord.crateWord(lines);
            }
        } catch (InvalidPasswordException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}
