package com.example.pdf.util;

import org.apache.poi.xwpf.usermodel.XWPFDocument;
import org.apache.poi.xwpf.usermodel.XWPFParagraph;
import org.apache.poi.xwpf.usermodel.XWPFRun;

import java.io.File;
import java.io.FileOutputStream;

/***
 * 生成word文件，存放到临时文件夹中
 *  */
public class TransformationWord {

    public String crateWord(String [] data){
        try {
            XWPFDocument doc = new XWPFDocument();

            for (String str : data) {
                XWPFParagraph docParagraph = doc.createParagraph();
                XWPFRun run = docParagraph.createRun();
                run.setText(str);
            }
//            将读取到的文件输出到临时文件夹
            FileUtils file=new FileUtils();
            File tempFile = file.createTempFile();
            doc.write(new FileOutputStream(tempFile));
            return tempFile.getPath();
        }catch (Exception e){
            e.printStackTrace();
            return null;
        }

    }
}
