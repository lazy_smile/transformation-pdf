package com.example.pdf.util;

public class FileUtils {
    public static java.io.File createTempFile() throws Exception{
        String prefix = "test";
        String suffix = ".docx" ;
        return java.io.File.createTempFile(prefix, suffix);
    }
    public static java.io.File getTempFile(String fileName) throws Exception{
        java.io.File tempFile = new java.io.File( System.getProperty("java.io.tmpdir"), fileName );
        if(!tempFile.exists()) throw new Exception("Temp file not found.");
        return tempFile;
    }
}
